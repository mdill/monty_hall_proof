# BASH alias compilation

## Purpose

This repo was birthed when an acquaintance attempted to "teach" me about Game
Theory.  This instruction came in the form of describing the Monty Hall problem.

In the Monty Hall problem a prize is hidden behind one of three doors.  A
contestant is given a blind-choice of one of the three doors, giving the
contestant a 1/3 (or 33.333%) chance of winning the hidden prize.  At this point
the host "removes" one of the "wrong" doors, which the contestant did not choose.
At this point the contestant can either keep their original choice or swap their
choice to the one remaining door -- but if the contestant **ALWAYS** swaps their
choice to the one remaining door, then their chances of winning the hidden prize
will be 2/3 (or 66.666%).

The issue is that, when this problem was presented to me, the scenario was
altered: instead of the contestant **ALWAYS** swapping their chosen door, the
contestant has a "choice to swap doors."  By having the contestant make this
second choice, statistics state that the chances of the contestant winning the
hidden prize will always be 50%.

This discrepancy spurred an argument over what that acquaintance thought they
**knew** about game theory, and what I **KNOW** about statistics.

In order to prove the mathematics about statistics, as well as the importance of
correct syntax, I created two scripts.  One script -- `forced_choice.cpp` -- is
designed to emulate the Monty Hall problem as it is supposed to be, complete
with the **mandatory** door swap after the host "removes" one of the incorrect
doors.  The second script -- `contestant_choice.cpp` -- is designed to emulate
the flawed version of the Monty Hall problem as that acquaintance posited it.

Both scripts run through 1000 iterations of the three-door game, all while
displaying correct/incorrect "doors," and finishing up by displaying the final
win-percentage of the overall test.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/monty_hall_proof.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](LICENSE.txt) file for
details.

