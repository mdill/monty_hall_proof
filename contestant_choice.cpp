#include <stdlib.h>             // For rand()
#include <iostream>             // For cout, endl
#include <iomanip>              // For setprecision(), fixed()

#define FIXED_FLOAT( num )  std::fixed << std::setprecision( 2 ) << num

// Returns a keep/change choice randomly
// 0 = keep choice
// 1 = change choice
bool returnChoice(){
    //return 1;                   // Always return 1 to change choice

    return std::rand() % 2;     // Return either 0 or 1 randomly
}

// Print the results of each round
void printRound( int winningDoor, int origChoice, int removedDoor,
                 int newChoice, bool wonGame, int round ){
    std::cout << "Round: " << round << "\t";
    std::cout << "Correct Door: " << winningDoor << "\t\t";
    std::cout << "Original Choice: " << origChoice << "\t";
    std::cout << "Removed Door: " << removedDoor << "\t\t";
    std::cout << "New Choice: " << newChoice << "\t\t";

    if( wonGame )
        std::cout << "Won: Y";
    else
        std::cout << "Won: N";

    std::cout << std::endl;
}

// Print the results of the entire simulation
void printResults( int wins ){
    std::cout << std::endl << "\t";
    std::cout << "Num of wins: " << wins << "\t";
    std::cout << "Num of losses: " << 1000 - wins << "\t";
    std::cout << "Win Percentage = " << FIXED_FLOAT( wins / 10 ) << "%";
    std::cout << std::endl << std::endl;
}

int main(){
    int winCount = 0;           // Track how many wins occur

    // Run the simulation 1000 times
    for( int i = 0; i < 1000; ++i ){
        // Assume the contestant loses until they win
        bool contestantWon = false;

        // Hide a prize behind a random door ( 1-3 )
        int winningDoor = std::rand() % 3 + 1;

        // Contestant chooses a random door ( 1-3 )
        int contestantChoice = std::rand() % 3 + 1;

        // Remove a "loser door" that the contestant didn't choose
        int removedDoor = 1;
        while( removedDoor == winningDoor || removedDoor == contestantChoice )
            ++removedDoor;

        // Change the contestant's choice based on "rules"
        int newContestantChoice;
        if( returnChoice() ){
            newContestantChoice = 6 - removedDoor - contestantChoice;
        }

        // If the contestant has chosen a winning door, they win
        if( newContestantChoice == winningDoor ){
            contestantWon = true;
            ++winCount;
        }

        // Wrap up this round
        printRound( winningDoor, contestantChoice,
                    removedDoor, newContestantChoice,
                    contestantWon, i + 1 );
    }

    printResults( winCount );           // Display overall percentages
}

